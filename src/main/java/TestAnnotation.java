import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class TestAnnotation {

    public static void main(String[] args) {
        Student student = new Student();

        Class cls = student.getClass();
        Constructor[] constructors = cls.getConstructors();
        Field[] fields = cls.getFields();

        for(Constructor constructor: constructors) {
            System.out.println(constructor.getName());
        }

        for(Field field: fields) {
            System.out.println(field.getAnnotations());
        }

        System.out.println(student);
    }
}

