public class Student {
    private String name = " ";

    @OwnAnnotation(studentAge = 14, name = "Dmytro")
    @Override
    public String toString() {
        return "Student: "  + name;
    }
}
